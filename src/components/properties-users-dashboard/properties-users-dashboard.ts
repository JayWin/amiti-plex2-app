import { Component, OnInit } from '@angular/core';
import { NavController } from "ionic-angular";
import { EntitiesProvider } from '../../providers/entities/entities';
import { Subscription } from "rxjs/Subscription";
import { Property } from '../../models/property';
import { User } from '../../models/user';
import { PropertiesProvider } from '../../providers/properties/properties';
import 'rxjs/add/operator/take';
import { faBuilding, faUser } from '@fortawesome/free-solid-svg-icons';
import { UsersProvider } from '../../providers/users/users';
import { PropertiesListComponent } from '../../pages/plex/properties/properties-list/properties-list';
import { UsersListComponent } from '../../pages/plex/users/users-list/users-list';

@Component({
  selector: 'properties-users-dashboard',
  templateUrl: 'properties-users-dashboard.html'
})
export class PropertiesUsersDashboardComponent implements OnInit {
  faBuilding = faBuilding;
  faUser = faUser;

  usersCount: number;
  propertiesCount: number;
  subscription: Subscription;

  propertySubscription: Subscription;
  properties: Property[];

  usersSubscription: Subscription;
  users: User[];

  constructor(
    public navCtrl: NavController,
    private entitiesProvider: EntitiesProvider,
    private propertiesProvider: PropertiesProvider,
    private usersProvider:  UsersProvider
  ) {

  }

  ngOnInit() {
    this.subscription = this.entitiesProvider.getEntityCounts().subscribe(countData => {
        this.usersCount = countData.usersCount;
        this.propertiesCount = countData.propertiesCount;
        this.propertySubscription = this.propertiesProvider.fetchEntityProperties().take(1).subscribe(propertiesData => {
          this.properties = propertiesData;
          console.log(this.properties);
        });

        this.usersSubscription = this.usersProvider.fetchEntityUsers().subscribe(entityUsersData => {
          console.log(entityUsersData);
        });
    });
  }

  openProperties() {
    this.navCtrl.push(PropertiesListComponent);
  }

  openUsers() {
    this.navCtrl.push(UsersListComponent);
  }

}
