import { NgModule } from '@angular/core';

import { IonicModule } from 'ionic-angular';
import { CommonModule } from '@angular/common';
import { GoogleLoginComponent } from './google-login/google-login';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PropertiesUsersDashboardComponent } from './properties-users-dashboard/properties-users-dashboard';

@NgModule({
	declarations: [GoogleLoginComponent,
    PropertiesUsersDashboardComponent],
	imports: [
		CommonModule, // <--- for angular directives
    IonicModule, // <--- for ionic components
    FontAwesomeModule
	],
	exports: [GoogleLoginComponent,
    PropertiesUsersDashboardComponent]
})
export class ComponentsModule {}
