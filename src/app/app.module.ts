import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { GooglePlus } from '@ionic-native/google-plus'; // We'll install this in the next section

import { firebaseConfig } from './credentials';
import { EntitiesProvider } from '../providers/entities/entities';
import { AuthProvider } from '../providers/auth/auth';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ComponentsModule } from '../components/components.module';
import { PlexModule } from '../pages/plex/plex.module';
import { PropertiesProvider } from '../providers/properties/properties';
import { UsersProvider } from '../providers/users/users';


@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    ComponentsModule,
    FontAwesomeModule,
    PlexModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    GooglePlus,
    StatusBar,
    SplashScreen,
    AuthProvider,
    EntitiesProvider,
    PropertiesProvider,
    UsersProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
