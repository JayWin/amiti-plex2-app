export interface Module {
    id?: string;
    name?: string;
    description?: string;
    active?: boolean
}
