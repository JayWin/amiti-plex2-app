export interface Mod {
  id?: string;
  name?: string;
  description?: string;
  active?: boolean;
  requiresConfig?: boolean;
  weevrUrl?: string;
}
