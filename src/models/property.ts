export class Property {
    constructor(
        public id?: string,
        public uid?: string,
        public property_number?: string,
        public section_stand_number?: string,
        public door_number?: string,
        public street_name?: string,
        public created_by?: string,
        public created?: Date,
        public modified?: Date,
        public active?: boolean,
        public type?: string,
        public occupant?: boolean,
        public legal_status?: string,
        public legal_standing?: string,
        public transfer_date?: string,
        public bondholder?: string,
        public bondValue?: string,
        public registered_owner?: string,
        public hasUsersCell?: boolean,
        public registered_owner_ref?: string
    ) { }
}

export class HistoryEntry {
    constructor(
        public id?: string,
        public propertyId?: string,
        public userId?: string,
        public full_name?: string,
        public created?: Date,
        public changed?: any
    ) { }
}