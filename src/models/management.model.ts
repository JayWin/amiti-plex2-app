export class ManagementUser {
    constructor(
        public uid?: string,
        public firstname?: string,
        public surname?: string,
        public email?: string,
        public cell?: string,
        public created_by?: string,
        public created?: Date,
        public modified?: Date,
        public status?: boolean,
        public ref?: any,
        public isAdmin?: boolean,
        public isSchemeExecutive?: boolean,
        public isViewOnly?: boolean,
        public receiveUserUpdates?: boolean,
        public profiles?: any,
        public online?: boolean
    ) { }
}
