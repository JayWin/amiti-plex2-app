export interface User {
  id?: string,
  uid?: string,
  isAuthorized?: boolean,
  firebaseId?: string,
  title?: string,
  firstname?: string,
  surname?: string,
  email?: string,
  password?: string,
  tel?: any,
  cell?: any,
  home?: string,
  fax?: string,
  idpassport?: number,
  address?: string,
  suburb?: string,
  city?: string,
  province?: string,
  code?: string,
  postal_address?: string,
  postal_suburb?: string,
  postal_city?: string,
  postal_province?: string,
  postal_code?: string,
  legalstatus?: string,
  created_by?: string,
  created?: Date,
  modified?: Date,
  active?: boolean,
  verified?: boolean,
  socialLogin?: boolean,
  token?: string,
  ref?: any,
  type?: string,
  displayName?: string,
  photoURL?: string,
  occupant?: boolean,
  disabled?: boolean,
  receive_accounts_communication?: boolean,
  receive_owner_communication?: boolean,
  receive_non_scheme_executive_communication?: boolean,
  receive_user_communication?: boolean,
  country?: string,
  nationality?: string,
  primary?: boolean,
  online?: boolean,
}
