import { Injectable } from "@angular/core";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFirestore } from "angularfire2/firestore";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/switchMap";
import "rxjs/add/operator/toPromise";
import "rxjs/add/operator/switchMap";
import firebase from "firebase/app";
import { User } from "../../models/user";

@Injectable()
export class AuthProvider {
  user: Observable<User>;
  userDetails: User;
  public userId: string;
  public userName: string;
  emailVerified: boolean = false;

  constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore) {
    this.user = this.afAuth.authState.switchMap(user => {
      if (user) {
        if (!this.userDetails) {
          const userDoc = this.afs.collection("userRefs").doc<User>(user.uid).ref;

          userDoc.get().then(doc => {
            if (doc.exists) {
              this.userDetails = doc.data();
              this.userId = this.userDetails.uid;
              sessionStorage.setItem("userId", this.userId);
              sessionStorage.setItem("userFirstname", this.userDetails.firstname);
              sessionStorage.setItem("userSurname", this.userDetails.surname);
            }
          });
        }
        return this.afs.doc<User>(`userRefs/${user.uid}`).valueChanges();
      } else {
        return Observable.of(null);
      }
    });
  }

  emailSignUp(email, password, firstname, surname) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password).then(user => {
      user.firstname = firstname;
      user.surname = surname;
    });
  }

  emailLogin(email, password) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  resetPassword(email) {
    const fbAuth = firebase.auth();
    return fbAuth.sendPasswordResetEmail(email);
  }

  updateUserPassword(user) {
    const currentUser = this.afAuth.auth.currentUser;
    return currentUser
      .updatePassword(user.password)
      .then(() => {
        console.log("password successful");
      })
      .catch(error => {
        console.log(error);
      });
  }

  signOut() {
    this.afAuth.auth.signOut().then(() => {
      console.log("logged out user id", this.userId);
      this.updateOnlineStatus(this.userId, false);
    });
  }

  checkPendingUsersInvites(user: User) {
    let collref = this.afs.collection("pendingUserInvites").ref;
    let queryref = collref.where("email", "==", user.email);
    return queryref.get().then(snapShot => {
      if (snapShot.empty) {
        return Promise.reject("No pending invites");
      } else {
        return Promise.resolve(
          snapShot.docs.map(documentSnapshot => {
            return documentSnapshot.data();
          })
        );
      }
    });
  }

  updateOnlineStatus(userId: string, status: boolean) {
    const userRef = this.afs.doc(`users/${userId}`);

    userRef.set(
      {
        online: status
      },
      { merge: true }
    );

    this.userId = userId;
  }
}
