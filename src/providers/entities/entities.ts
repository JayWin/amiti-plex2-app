import { Injectable } from "@angular/core";
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from "angularfire2/firestore";
import { Subject } from "rxjs/Subject";

import { Entity } from "../../models/entity";
import { Subscription } from "rxjs/Subscription";
import { AuthProvider } from "../auth/auth";

@Injectable()
export class EntitiesProvider {
  entitiesCollection: AngularFirestoreCollection<Entity>;
  entityDoc: AngularFirestoreDocument<Entity>;
  entityTemp: AngularFirestoreDocument<any>;
  entity = new Subject<Entity>();
  currentEntity: Entity;
  userID: string;

  permissions = new Subject();
  permissionsList: any;
  userPermissions: any;
  permissionsSubscription: Subscription;

  sponsorsCollection: AngularFirestoreCollection<any>;
  galleryCollection: AngularFirestoreCollection<any>;
  deleteImage: AngularFirestoreDocument<any>;
  itemCollection: AngularFirestoreCollection<Entity>;
  progressBarValue;

  constructor(
    public afStore: AngularFirestore,
    private auth: AuthProvider
  ) {
    this.auth.user.subscribe(user => {
      if (user) {
        this.userID = user.uid;
        console.log(this.userID);
        this.entitiesCollection = this.afStore.collection(`users/${user.uid}/entities`, ref => ref.orderBy("name", "asc"));
      }
    });
  }

  getEntities(userID) {
    this.entitiesCollection = this.afStore.collection(`users/${userID}/entities`, ref => ref.orderBy("name", "asc"));
    return this.entitiesCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data();
        console.log("entities data", data);
        data.id = a.payload.doc.id;

        // FETCH ENTITY LOGO
        this.afStore
          .doc(`entities/${a.payload.doc.id}`)
          .snapshotChanges()
          .subscribe(snap => {
            data.template = snap.payload.data().template;
            data.deployed = snap.payload.data().deployed;
          });

        // FETCH ENTITY PERMISSIONS
        let isAdmin = false;
        let isViewOnly = false;

        if (data.permissions) {
          if (data.permissions.indexOf("admin") !== -1) {
            isAdmin = true;
          }
          if (data.permissions.indexOf("view_only") !== -1) {
            isViewOnly = true;
          }
        }

        data.isAdmin = isAdmin;
        data.isViewOnly = isViewOnly;

        return data;
      });
    });
  }

  // filterEntities() {
  //   return this.getEntities();
  // }

  // filterEntitiesType(type: string) {
  //   const userID = sessionStorage.getItem("userId");

  //   if (type === "") {
  //     this.entitiesCollection = this.afStore.collection(`users/${userID}/entities`, ref => ref.orderBy("name", "asc"));
  //   } else {
  //     this.entitiesCollection = this.afStore.collection(`users/${userID}/entities`, ref => ref.where("entity_type", "==", type));
  //   }
  //   return this.getEntities();
  // }

  // resetEntities() {
  //   const userID = sessionStorage.getItem("userId");

  //   this.entitiesCollection = this.afStore.collection(`users/${userID}/entities`, ref => ref.orderBy("name", "asc"));
  //   return this.getEntities();
  // }

  //FETCH ENTITY DETAILS
  fetchEntityDetails(entityId: String) {
    this.entityDoc = this.afStore.doc(`entities/${entityId}`);
    return this.entityDoc.valueChanges();
  }

  getEntity(entityId) {
    return this.afStore.doc(`entities/${entityId}`).valueChanges();
  }

  getUsersForEntity() {
    const entityID = sessionStorage.getItem("entity_id");
    return this.afStore
      .collection(`entities/${entityID}/users`, ref => ref.orderBy("firstname", "asc").where("active", "==", true))
      .snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data();
          data.id = a.payload.doc.id;
          return data;
        });
      });
  }

  getPropertiesForEntity() {
    const entityID = sessionStorage.getItem("entity_id");
    return this.afStore.collection(`entities/${entityID}/properties`, ref => ref.orderBy("property_number", "asc").where("active", "==", true)).valueChanges();
  }

  getSubEntitiesForEntity() {
    const entityID = sessionStorage.getItem("entity_id");
    return this.afStore.collection(`entities/${entityID}/subentities`).valueChanges();
  }

  setEntity(entity: Entity) {
    const userID = sessionStorage.getItem("userId");

    this.entity.next(entity);
    this.currentEntity = entity;
    sessionStorage.setItem("entity_id", entity.id);
    sessionStorage.setItem("entity_name", entity.name);

    // UNSUBSCRIBE FROM PREVIOUS ENTITY PERMISSIONS AND FLUSH OUT PERMISSIONS
    if (this.permissionsSubscription) {
      this.permissionsSubscription.unsubscribe();
      sessionStorage.removeItem("permissions");
    }

    // FETCH ENTITY PERMISSIONS
    this.permissionsSubscription = this.getUserPermissionsForEntity(userID).subscribe((userEntityPermissions: any) => {
      if (userEntityPermissions.permissions) {
        this.permissions.next(userEntityPermissions.permissions);
        sessionStorage.setItem("permissions", JSON.stringify(userEntityPermissions.permissions));
      }
    });
  }

  getCurrentEntityID() {
    return sessionStorage.getItem("entity_id");
  }

  getCurrentEntityName() {
    return sessionStorage.getItem("entity_name");
  }

  addEntity(entity: Entity) {
    entity.deployed = false;
    entity.active = true;
    entity.createdby = sessionStorage.getItem("userId");
    return this.afStore.collection(`entities`).add(entity);
  }

  updateEntity(entity: Entity) {
    this.entityDoc = this.afStore.doc(`entities/${entity.id}`);
    return this.entityDoc.update(entity).then(() => {
      //ADD LOG ENTRY
      let logData = {
        name: entity.name,
        description: "Entity details updated",
        type: "update",
        category: "entities",
        created: Date.now()
      };
    });
  }

  updateEntityTemplate(entityID, entityTemplate: any, entityContactPersonDetails: any, entitySocialLinks: any) {
    this.entityTemp = this.afStore.doc(`entities/${entityID}`);
    entityTemplate.contact_person_details = entityContactPersonDetails;
    entityTemplate.social_links = entitySocialLinks;
    this.entityTemp.set(
      {
        template: entityTemplate
      },
      { merge: true }
    );
  }

  updateEntityContact(primaryContactId) {
    const entityID = sessionStorage.getItem("entity_id");
    const entityContact = this.afStore.doc(`entities/${entityID}`);
    entityContact.set(primaryContactId, { merge: true });
  }

  getEntityCounts() {
    const entityID = sessionStorage.getItem("entity_id");
    this.entityDoc = this.afStore.doc(`entities/${entityID}`);
    return this.entityDoc.valueChanges();
  }

  collectData(entity: Entity) {
    this.itemCollection = this.afStore.collection("entities", ref => {
      return ref.where("domain", "==", entity.domain);
    });
    return this.afStore
      .collection(`entities`, ref => ref.orderBy("name", "asc"))
      .snapshotChanges()
      .map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data() as Entity;
          data.id = a.payload.doc.id;
          return data;
        });
      });
  }

  fetchSponsorImages(entityID) {
    this.sponsorsCollection = this.afStore.collection(`entities/${entityID}/sponsors`, ref => ref.orderBy("name", "asc"));

    return this.sponsorsCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as any;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  fetchGalleryImages(entityID) {
    this.galleryCollection = this.afStore.collection(`entities/${entityID}/gallery`, ref => ref.orderBy("name", "asc"));

    return this.galleryCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as any;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  //PERMISSIONS
  getUserPermissionsForEntity(userId: string) {
    const entityID = this.getCurrentEntityID();
    return this.afStore.doc(`/users/${userId}/entities/${entityID}`).valueChanges();
  }

  fetchEntityModules() {
    const entityID = sessionStorage.getItem("entity_id");

    return this.afStore.collection(`entities/${entityID}/modules`).valueChanges();
  }
}
