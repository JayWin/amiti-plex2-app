import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { User } from '../../models/user';
import { Property, HistoryEntry } from '../../models/property';
import { ManagementUser } from '../../models/management.model';

@Injectable()
export class UsersProvider {
    usersCollection: AngularFirestoreCollection<User[]>;
    entityUsersCollection: AngularFirestoreCollection<any>;
    userDoc: AngularFirestoreDocument<User>;
    user = new Subject<User>();
    currentUser: User;
    currentUsers: User[];
    propertiesCollection: AngularFirestoreCollection<Property[]>;
    propertyDoc: AngularFirestoreDocument<Property>;
    propertyOtherDoc: AngularFirestoreDocument<Property>;
    permissionDoc: AngularFirestoreDocument<any>;
    managementUsersList: Observable<any>;

    constructor(public afs: AngularFirestore) { }

    addUser(userData, location?: string) {
        const entityID = sessionStorage.getItem('entity_id');

        // CHECK IF USER EXISTS ON PLEX
        this.usersCollection = this.afs.collection('users', ref => ref.where('email', '==', userData.email));

        return this.usersCollection.snapshotChanges()
            .map(changes => {
                return changes.map(a => {
                    const data = a.payload.doc.data() as User;
                    return data;
                });
            }).take(1)
            .toPromise()
            .then(usersList => {
                if (usersList.length === 0) {
                    // ADD NEW USER
                    userData.status = true;
                    return this.usersCollection.add(userData)
                        .then(ref => {
                            return this.linkUserEntity(entityID, ref.id, userData)
                                .then(() => {
                                    return ref.id;
                                });
                        });
                } else {
                    // ADD EXISTING USER

                    // CHECK IF USER ALREADY ADDED TO ENTITY
                    this.entityUsersCollection = this.afs.collection('entities').doc(entityID).collection('users', ref => ref.where('uid', '==', usersList[0].uid));

                    return this.entityUsersCollection.snapshotChanges()
                        .map(changes => {
                            return changes.map(a => {
                                const data = a.payload.doc.data() as User;
                                data.uid = a.payload.doc.id;
                                return data.uid;
                            });
                        })
                        .take(1)
                        .toPromise()
                        .then(entityUsersList => {
                            if (entityUsersList.length === 0) {
                                // USER NOT ADDED TO ENTITY SO CAN ADD USER
                                return this.linkUserEntity(entityID, usersList[0].uid, usersList[0])
                                    .then(() => {
                                        return usersList[0].uid;
                                    });
                            } else {
                                // USER ALREADY ADDED TO ENTITY SO DISPLAY ERROR
                                if (location == 'userAddPage') {
                                }
                                return usersList[0].uid;
                            }
                        });
                }
            });
    }

    checkUserVerified(userUID: string) {
        const userRef = this.afs.collection('users').doc(userUID);

        return userRef.snapshotChanges().take(1).toPromise()
            .then(user => {
                const userData = user.payload.data();

                if (!userData.verified) {
                    console.log('redirect to user');
                }
            });
    }

    linkUserEntity(entityID, userID, userData) {
        console.log('Add user', entityID, userID, userData);
        const entityRef = this.afs.collection('entities').doc(entityID);
        const userRef = this.afs.collection('users').doc(userID);
        const pendingUserInvitesRef = this.afs.collection('pendingUserInvites').doc(userID);
        const pendingEmailEntityInviteRef = this.afs.collection('pendingEmailEntityInvites');
        const userRefsRef = this.afs.collection('userRefs', ref => ref.where('email', '==', userData.email));
        let usersCount = 0;

        // LINK ENTITY TO USER
        const linkEntityToUser = entityRef.snapshotChanges().take(1).toPromise()
            .then(snap => {
                const entityDetails = snap.payload.data();
                usersCount = entityDetails.usersCount;

                userRef.collection('entities').doc(entityID)
                    .set({
                        name: entityDetails.name,
                        ref: entityRef.ref,
                        uid: entityID,
                        entity_type: entityDetails.entity_type,
                        domain: entityDetails.domain,
                        active: true
                    }, { merge: true });

                // INCREMENT USERS COUNT
                usersCount++;
                entityRef.set({
                    usersCount: usersCount
                }, { merge: true });
            })

        // LINK USER TO ENTITY
        if (!userData.cell) {
            userData.cell = '';
        }

        const linkUserToEntity = entityRef.collection('users').doc(userID).set({
            firstname: userData.firstname,
            surname: userData.surname,
            email: userData.email,
            cell: userData.cell,
            ref: userRef.ref,
            uid: userID,
            active: true
        }, { merge: true });

        // CREATE PENDING USER INVITE
        const createPendingUserInvite = userRefsRef.snapshotChanges()
            .map(changes => {
                return changes.map(a => {
                    const data = a.payload.doc.data() as User;
                    data.uid = a.payload.doc.id;
                    return data.uid;
                });
            })
            .take(1)
            .toPromise()
            .then(userRefsData => {
                if (userRefsData.length === 0) {
                    // IF USER REF DOES NOT EXIST CREATE ONE
                    return pendingUserInvitesRef.set({
                        firstname: userData.firstname,
                        surname: userData.surname,
                        email: userData.email,
                        cell: userData.cell,
                        ref: userRef.ref,
                        uid: userID,
                    }, { merge: true });
                }
            });

        // CREATE PENDING EMAIL ENITY INVITE
        const createPendingEmailEntityInvite = pendingEmailEntityInviteRef.add({
            entityId: entityID,
            userId: userID,
            //environmentClient: environment.client
        });

        return Promise.all([linkEntityToUser, linkUserToEntity, createPendingUserInvite, createPendingEmailEntityInvite]).then(() => {
            // this.router.navigate(['/users']);
            return Promise.resolve(userID);
        });
    }

    fetchEntityUsers() {
        const entityID = sessionStorage.getItem('entity_id');

        this.entityUsersCollection = this.afs.collection(`entities/${entityID}/users`, ref => ref.where('active', '==', true));
        const usersRef = this.afs.doc(`entities/${entityID}`).ref;
        return this.entityUsersCollection.snapshotChanges().map(changes => {
            usersRef.update({
                usersCount: Number(changes.length)
            });
            return changes.map(a => {
                const data = a.payload.doc.data() as User;
                return data;
            });
        });
    }

    fetchEntityUnregisteredUsers() {
        const entityID = sessionStorage.getItem('entity_id');
        const pendingUserInvitesRef = this.afs.collection(`pendingUserInvites`);

        return pendingUserInvitesRef.valueChanges();
    }

    setEntityUserInactive(user, usersListCount) {
        const entityID = sessionStorage.getItem('entity_id');
        const entityRef = this.afs.collection('entities').doc(entityID);
        return this.afs.collection(`entities`).doc(entityID).collection('users').doc(user.uid)
            .update({
                active: false
            }).then((result) => {
                let logData = {
                    name: user.email,
                    description: 'User was removed',
                    type: 'remove',
                    category: 'user',
                    created: Date.now()
                }

                // entityRef.snapshotChanges().take(1).toPromise()
                //     .then((entityDetails) => {
                //         const entityData = entityDetails.payload.data();
                //         usersListCount--;
                //         return this.afs.collection('entities').doc(entityID).update({
                //             usersCount: usersListCount
                //         }).then(() => {
                //             let logData = {
                //                 name: user.email,
                //                 description: 'User was removed',
                //                 type: 'remove',
                //                 category: 'user',
                //                 created: Date.now()
                //             }
                //             this.auditLogService.addAudit(logData);
                //         });
                //     });

            });


    }

    fetchUserProperties(userUID: string) {
        const entityID = sessionStorage.getItem('entity_id');

        this.propertiesCollection = this.afs.collection('users').doc(userUID).collection('entities').doc(entityID).collection('properties', ref => ref.where('active', '==', true).orderBy('property_number', 'asc'));

        return this.propertiesCollection.snapshotChanges().map(changes => {
            return changes.map(a => {
                const data = a.payload.doc.data();
                data.uid = data.ref.id;
                return data;
            });
        });
    }

    fetchUser(userId: string) {
        this.userDoc = this.afs.doc(`users/${userId}`);
        return this.userDoc.valueChanges();
    }

    fetchUserDetails(ref: string) {
        this.userDoc = this.afs.doc(ref);
        return this.userDoc.valueChanges();
    }

    fetchUserDetailsByEmail(email: string) {
        this.usersCollection = this.afs.collection('users', ref => ref.where('email', '==', email));

        return this.usersCollection.valueChanges();
    }

    setEditUser(refPath: string) {
        this.fetchUserDetails(refPath).subscribe(userDetails => {
            userDetails.ref = refPath;
            this.user.next(userDetails);
            this.currentUser = userDetails;
        });
    }

    updateUser(user: User) {
        this.userDoc = this.afs.collection('users').doc(user.uid);
        return this.userDoc.update(user).then(() => {
            let logData = {
                userId: user.uid,
                name: user.email,
                description: 'User was updated',
                type: 'update',
                category: 'user',
                created: Date.now()
            }
        });
    }

    updateUserPropertyDetails(propertyData, userUID) {
        const entityID = sessionStorage.getItem('entity_id');
        this.propertyDoc = this.afs.collection('users').doc(userUID).collection('entities').doc(entityID).collection('properties').doc(propertyData.uid);
        //CHECK TO UPDATE ON BOTH PLACES
        this.propertyOtherDoc = this.afs.collection('entities').doc(entityID).collection('properties').doc(propertyData.uid).collection('users').doc(userUID);
        this.propertyOtherDoc.update(propertyData);
        return this.propertyDoc.update(propertyData).then(() => {
        });
    }

    addHistoryLogToUser(userLog, userId) {
        const entityID = sessionStorage.getItem('entity_id');
        const loggedInUserId = sessionStorage.getItem('userId');
        const propertyHistoryCollection = this.afs.collection(`entities/${entityID}/users/${userId}/history`);
        let logData = {
            userId: userId,
            created: Date.now(),
            changed: userLog,
            changedBy: loggedInUserId,
            // client: environment.client,
            // admin: environment.admin
        }
        return propertyHistoryCollection.add(logData).then((result) => {
            this.addPendingUserUpdates(logData);
        }).catch((err) => {
            console.log('Property history error');
        });
    }

    addPendingUserUpdates(logData) {
        const entityID = sessionStorage.getItem('entity_id');
        const pendingUpdatesCollection = this.afs.collection(`pendingNotifyReceiveUserUpdates`);
        logData.entityId = entityID;
        pendingUpdatesCollection.add(logData).catch((err) => {
            console.log(err);
        });
    }

    fetchUserHistory(userId) {
        const entityID = sessionStorage.getItem('entity_id');
        const historyCollection = this.afs.collection(`entities/${entityID}/users/${userId}/history`, ref => ref.orderBy('created', 'desc'));

        return historyCollection.snapshotChanges().map(changes => {
            return changes.map(a => {
                const data = a.payload.doc.data() as HistoryEntry;
                data.id = a.payload.doc.id;
                return data;
            });
        });
    }

    fetchUserHistoryUser(userId) {
        const userDoc = this.afs.doc(`users/${userId}`);
        return userDoc.valueChanges();

    }

    updateUserPermission(userUID, permissions: any) {
        const entityID = sessionStorage.getItem('entity_id');

        const data = {
            permissions: permissions
        }

        this.permissionDoc = this.afs.collection('users').doc(userUID).collection('entities').doc(entityID);
        this.permissionDoc.update(data);
    }

    addPropertyToUser(userID: string, propertyID: string, type: string, occupant: boolean) {
        const entityID = sessionStorage.getItem('entity_id');
        const userRef = this.afs.collection('users').doc(userID);
        const propertyRef = this.afs.collection('entities').doc(entityID).collection('properties').doc(propertyID);
        let userData: User = {};

        console.log('type', type);

        // FETCH USER DETAILS
        return userRef.snapshotChanges().take(1).toPromise()
            .then(userDetails => {
                if (userDetails.payload.data()) {
                    userData = userDetails.payload.data();

                    // CHECK IF USER ALREADY ADDED TO PROPERTY
                    return propertyRef.collection('users', ref => ref.where('uid', '==', userData.uid).where('active', '==', true)).snapshotChanges()
                        .map(changes => {
                            return changes.map(a => {
                                const data = a.payload.doc.data() as User;
                                data.uid = a.payload.doc.id;
                                return data.uid;
                            });
                        })
                        .take(1)
                        .toPromise()
                        .then(usersUidCheckCollection => {
                            if (usersUidCheckCollection.length === 0) {
                                console.log('User does not exists with uid - ', userData.uid);

                                if (type !== 'tenant') {
                                    console.log('Type not tenant so do type check');

                                    // CHECK IF TYPE ALREADY EXISTS
                                    return propertyRef.collection('users', ref => ref.where('type', '==', type).where('active', '==', true)).snapshotChanges()
                                        .map(changes => {
                                            return changes.map(a => {
                                                const data = a.payload.doc.data() as User;
                                                data.uid = a.payload.doc.id;
                                                return data.uid;
                                            });
                                        })
                                        .take(1)
                                        .toPromise()
                                        .then(usersCollection => {
                                            if (usersCollection.length === 0) {
                                                // ADD PROPERTY REFERENCE TO USER PROPERTIES COLLECTION
                                                console.log('type does not exist');
                                                return this.associatePropertyToUser(propertyID, userID, type, occupant, userData);
                                            } else {
                                                console.log('Type already exists');
                                                return Promise.reject(`A ${type} contact already exists on this property`);
                                            }
                                        })
                                        .catch(error => {
                                            console.log('No users found wih that type - ', error);
                                        });
                                } else {
                                    console.log('Type tenant so associate user to property');
                                    return this.associatePropertyToUser(propertyID, userID, type, occupant, userData);
                                }
                            } else {
                                console.log('User already exists');
                                return Promise.reject(`User already added to this property`);
                            }
                        })
                        .catch(error => {
                            console.log('User UID check error - ', error);
                        });

                } else {
                    console.log("No user found!");
                }
            }).catch(function(error) {
                console.log("Error getting user:", error);
            });
    }

    associatePropertyToUser(propertyID, userID, type, occupant, userData) {
        const entityID = sessionStorage.getItem('entity_id');
        const userRef = this.afs.collection('users').doc(userID);
        const propertyRef = this.afs.collection('entities').doc(entityID).collection('properties').doc(propertyID);

        const addPropertyToUser = propertyRef.snapshotChanges().take(1).toPromise()
            .then(propertyDetails => {
                if (propertyDetails.payload.data()) {
                    const propertyData = propertyDetails.payload.data();

                    // ASSOCIATE PROPERTY TO USER
                    userRef.collection('entities').doc(entityID).collection('properties').doc(propertyID)
                        .set({
                            property_number: propertyData.property_number,
                            ref: propertyRef.ref,
                            uid: propertyID,
                            type: type,
                            occupant: occupant,
                            active: true
                        }, { merge: true });

                } else {
                    console.log("No property found!");
                }
            }).catch(function(error) {
                console.log("Error getting property:", error);
            });

        // SET BLANK CELL NUMBER IF UNDEFINED
        if (!userData.cell) {
            userData.cell = '';
        }

        // ADD USER REFERENCE TO PROPERTY USERS COLLECTION
        const addUserToProperty = propertyRef.collection('users').doc(userID)
            .set({
                firstname: userData.firstname,
                surname: userData.surname,
                email: userData.email,
                cell: userData.cell,
                ref: userRef.ref,
                uid: userID,
                type: type,
                occupant: occupant,
                active: true
            }, { merge: true });

        return Promise.all([addPropertyToUser, addUserToProperty])
            .then(() => {
                // SET PROPERTY PRIMARY OR ALTERNATE OR TENANT USER FOR TABLE
                const fullname = userData.firstname + ' ' + userData.surname;
                let typeObj = {};

                typeObj[type] = fullname;
                propertyRef.set(typeObj, { merge: true });
            })
    }

    removePropertyFromUser(userID: string, propertyID: string, type: string) {
        const entityID = sessionStorage.getItem('entity_id');
        const userRef = this.afs.collection('users').doc(userID);
        const propertyRef = this.afs.collection('entities').doc(entityID).collection('properties').doc(propertyID);

        const deactivateUserProperty = userRef.collection('entities').doc(entityID).collection('properties').doc(propertyID)
            .set({
                active: false
            }, { merge: true });

        const deactivatePropertyUser = propertyRef.collection('users').doc(userID)
            .set({
                active: false
            }, { merge: true });

        return Promise.all([deactivateUserProperty, deactivatePropertyUser])
            .then(() => {
                if (type === 'tenant') {
                    // FETCH ALTERNATE TENANT DETAILS IF ONE EXISTS ELSE SET TO BLANK FOR TENANT TYPE
                    propertyRef.collection('users', ref => ref.where('type', '==', 'tenant').where('active', '==', true)).snapshotChanges()
                        .map(changes => {
                            return changes.map(a => {
                                const data = a.payload.doc.data() as User;
                                data.uid = a.payload.doc.id;
                                return data;
                            });
                        })
                        .take(1)
                        .toPromise()
                        .then(tenantCollection => {
                            if (tenantCollection.length === 0) {
                                propertyRef.set({
                                    tenant: ''
                                }, { merge: true });
                            } else {
                                tenantCollection.forEach(tenant => {
                                    propertyRef.set({
                                        tenant: tenant.firstname + ' ' + tenant.surname
                                    }, { merge: true });
                                });
                            }
                        });
                } else {
                    let typeObj = {};

                    typeObj[type] = '';
                    propertyRef.set(typeObj, { merge: true });
                }
            });
    }


    fetchUsers() {
        const entityID = sessionStorage.getItem('entity_id');

        this.usersCollection = this.afs.collection(`entities/${entityID}/management/users/list`);

        return this.managementUsersList = this.usersCollection.snapshotChanges().map(changes => {
            return changes.map(a => {
                const data = a.payload.doc.data() as ManagementUser;
                return data;
            });
        });
    }


    fetchPrimaryUser(userID) {
        const entityID = sessionStorage.getItem('entity_id');

        const primaryUser = this.afs.doc(`entities/${entityID}/management/users/list/${userID}`);

        return primaryUser.valueChanges();
    }

    fetchManagementTeam() {
        const entityID = sessionStorage.getItem('entity_id');
        return this.afs.collection(`entities/${entityID}/management/users/list`, ref => ref.orderBy('firstname', 'asc')).snapshotChanges().map(changes => {
            return changes.map(a => {
                const data = a.payload.doc.data();
                data.id = a.payload.doc.id;
                return data;
            });
        });
    }


    // ADD INFO STEPS

    fetchEntityUser(uid) {
        const entityID = sessionStorage.getItem('entity_id');
        const entityUserDetails = this.afs.doc(`users/${uid}/entities/${entityID}`);
        return entityUserDetails.valueChanges();

    }

    updateUserHelpSteps(user, userID) {
        console.log(user);
        let userData = {
            dashboardstep: user.dashboardstep,
            dashboardsteps: user.dashboardsteps
        }
        const entityID = sessionStorage.getItem('entity_id');
        const userSteps = this.afs.doc(`users/${userID}/entities/${entityID}`);
        return userSteps.set(userData, { merge: true });
    }
}
