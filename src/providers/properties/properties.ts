import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Injectable } from '@angular/core';

import { Property, HistoryEntry } from '../../models/property';
import { User } from '../../models/user';

@Injectable()
export class PropertiesProvider {
    entityPropertiesCollection: AngularFirestoreCollection<Property[]>;
    propertyDoc: AngularFirestoreDocument<Property>;
    usersCollection: AngularFirestoreCollection<User[]>;

    constructor(
      public afs: AngularFirestore,
    ) { }

    fetchEntityProperties() {
        const entityID = sessionStorage.getItem('entity_id');
        this.entityPropertiesCollection = this.afs.collection(`entities/${entityID}/properties`, ref => ref.where('active', '==', true).orderBy('property_number', 'asc'));
        const propertiesRef = this.afs.doc(`entities/${entityID}`).ref;
        return this.entityPropertiesCollection.snapshotChanges().map(changes => {
            propertiesRef.update({
                propertiesCount: Number(changes.length)
            });
            return changes.map(a => {
                const data = a.payload.doc.data() as Property;
                data.uid = a.payload.doc.id;
                return data;
            });
        });
    }

    setPropertyInactive(propertyData) {
        const entityID = sessionStorage.getItem('entity_id');
        const propertyId = propertyData.uid;
        console.log(propertyData);
        return this.afs.collection(`entities`).doc(entityID).collection('properties').doc(propertyId)
            .update({
                active: false
            }).then(() => {
                let logData = {
                    name: propertyData.property_number,
                    description: 'Property was removed',
                    type: 'remove',
                    category: 'properties',
                    created: Date.now()
                }
            }).catch((error) => {
            });
    }

    fetchPropertyUsers(propertyUID: string) {
        const entityID = sessionStorage.getItem('entity_id');
        this.usersCollection = this.afs.collection(`entities/${entityID}/properties/${propertyUID}/users`, ref => ref.where('active', '==', true));

        return this.usersCollection.snapshotChanges().map(changes => {
            return changes.map(a => {
                const data = a.payload.doc.data();
                return data;
            });
        });
    }

    fetchPropertyDetails(uid: string) {
        const entityID = sessionStorage.getItem('entity_id');
        this.propertyDoc = this.afs.doc(`entities/${entityID}/properties/${uid}`);
        return this.propertyDoc.valueChanges();
    }

    fetchRegisteredOwner(regOwnerId, propertyId) {
        const entityID = sessionStorage.getItem('entity_id');
        const ownerDoc = this.afs.collection(`entities/${entityID}/registered_owners`).doc(`${regOwnerId}`);
        return ownerDoc.valueChanges();

    }

    fetchAllRegisteredOwners() {
        const entityID = sessionStorage.getItem('entity_id');
        const ownersCollection = this.afs.collection(`entities/${entityID}/registered_owners`, ref => ref.where('active', '==', true));

        return ownersCollection.snapshotChanges().map(changes => {
            return changes.map(a => {
                const data = a.payload.doc.data();
                data.uid = a.payload.doc.id;
                return data;
            });
        });
    }

    addExistingRegOwnerToProperty(regOwnerId, propertyId) {
        console.log(`Add ${regOwnerId} to ${propertyId}`);
        const entityID = sessionStorage.getItem('entity_id');
        const propertyDoc = this.afs.collection(`entities/${entityID}/properties`).doc(`${propertyId}`);
        const ownerDoc = this.afs.collection(`entities/${entityID}/registered_owners/${regOwnerId}/properties`).doc(`${propertyId}`);
        return propertyDoc.set({ registered_owner: regOwnerId }, { merge: true }).then(() => {
            return ownerDoc.set({
                active: true,
                propertyId: propertyId,
                ownerId: regOwnerId
            }, { merge: true });
        });
    }

    removeRegisteredOwnerFromProperty(regOwnerId, propertyId) {
        console.log(`Remove ${regOwnerId} from ${propertyId}`);
        const entityID = sessionStorage.getItem('entity_id');
        const propertyDoc = this.afs.collection(`entities/${entityID}/properties`).doc(`${propertyId}`);
        const ownerDoc = this.afs.collection(`entities/${entityID}/registered_owners/${regOwnerId}/properties`).doc(`${propertyId}`);
        return propertyDoc.update({ registered_owner: '' }).then(() => {
            return ownerDoc.set({ active: false });
        });
    }

    updateProperty(property: Property) {
        console.log('property to update', property);
        const entityID = sessionStorage.getItem('entity_id');
        this.propertyDoc = this.afs.doc(`entities/${entityID}/properties/${property.uid}`);
        return this.propertyDoc.update(property).then(() => {
            let logData = {
                name: property.property_number,
                description: 'Property was updated',
                type: 'update',
                category: 'properties',
                created: Date.now()
            }
        }).catch((error) => {
        });
    }

    addHistoryLogToProperty(propertyLog, propertyId, userId) {
        const entityID = sessionStorage.getItem('entity_id');
        const propertyHistoryCollection = this.afs.collection(`entities/${entityID}/properties/${propertyId}/history`);
        let logData = {
            propertyId: propertyId,
            userId: userId,
            created: Date.now(),
            changed: propertyLog
        }
        return propertyHistoryCollection.add(logData).then((result) => {
            console.log('Success', result);
        }).catch((err) => {
            console.log('Property history error');
        });
    }

    fetchPropertyHistory(propertyId) {
        const entityID = sessionStorage.getItem('entity_id');
        const historyCollection = this.afs.collection(`entities/${entityID}/properties/${propertyId}/history`, ref => ref.orderBy('created', 'desc'));

        return historyCollection.snapshotChanges().map(changes => {
            return changes.map(a => {
                const data = a.payload.doc.data() as HistoryEntry;
                data.id = a.payload.doc.id;
                return data;
            });
        });
    }

    fetchPropertyHistoryUser(userId) {
        const userDoc = this.afs.doc(`users/${userId}`);
        return userDoc.valueChanges();

    }
}
