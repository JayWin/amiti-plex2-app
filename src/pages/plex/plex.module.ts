import { NgModule } from '@angular/core';

import { IonicModule } from 'ionic-angular';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../../components/components.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HomePage } from './home/home';
import { LoginPage } from './login/login';
import { DashboardPage } from './dashboard/dashboard';
import { PropertiesModule } from './properties/properties.module';
import { UsersModule } from './users/users.module';

@NgModule({
	declarations: [
    HomePage,
    LoginPage,
    DashboardPage
  ],
	imports: [
		CommonModule, // <--- for angular directives
    IonicModule, // <--- for ionic components
    FontAwesomeModule,
    ComponentsModule,
    PropertiesModule,
    UsersModule
	],
  entryComponents: [
    HomePage,
    LoginPage,
    DashboardPage
  ],
})
export class PlexModule {}
