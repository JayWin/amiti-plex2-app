import { Component, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";
import { EntitiesProvider } from "../../../../providers/entities/entities";
import { Subscription } from "rxjs/Subscription";
import { Property } from "../../../../models/property";
import { User } from "../../../../models/user";
import { PropertiesProvider } from "../../../../providers/properties/properties";
import "rxjs/add/operator/take";
import { faBuilding, faUser } from "@fortawesome/free-solid-svg-icons";
import { UsersProvider } from "../../../../providers/users/users";
import { UsersEditComponent } from "../users-edit/users-edit";

@Component({
  selector: "users-list",
  templateUrl: "users-list.html"
})
export class UsersListComponent implements OnInit {
  faUser = faUser;

  usersSubscription: Subscription;
  users: User[];

  constructor(public navCtrl: NavController, private entitiesProvider: EntitiesProvider, private propertiesProvider: PropertiesProvider, private usersProvider: UsersProvider) {}

  ngOnInit() {
    this.usersSubscription = this.usersProvider.fetchEntityUsers().subscribe(entityUsersData => {
      this.users = entityUsersData;
      console.log(entityUsersData);
    });
  }

  openLink(userID) {
    console.log(userID);
    this.navCtrl.push(UsersEditComponent, {
      userID: userID
    });
  }
}
