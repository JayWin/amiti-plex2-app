import { NgModule } from '@angular/core';

import { IonicModule } from 'ionic-angular';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../../../components/components.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { UsersListComponent } from './users-list/users-list';
import { UsersEditComponent } from './users-edit/users-edit';

@NgModule({
	declarations: [
    UsersListComponent,
    UsersEditComponent
  ],
	imports: [
		CommonModule, // <--- for angular directives
    IonicModule, // <--- for ionic components
    FontAwesomeModule,
    ComponentsModule
	],
  entryComponents: [
    UsersListComponent,
    UsersEditComponent
  ],
})
export class UsersModule {}
