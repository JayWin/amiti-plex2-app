import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController, NavParams } from "ionic-angular";
import { EntitiesProvider } from '../../../../providers/entities/entities';
import { Subscription } from "rxjs/Subscription";
import { Property } from '../../../../models/property';
import { User } from '../../../../models/user';
import { PropertiesProvider } from '../../../../providers/properties/properties';
import 'rxjs/add/operator/take';
import { faBuilding, faUser } from '@fortawesome/free-solid-svg-icons';
import { UsersProvider } from '../../../../providers/users/users';

@Component({
  selector: 'users-edit',
  templateUrl: 'users-edit.html'
})
export class UsersEditComponent implements OnInit, OnDestroy {

  userSubscription: Subscription;
  user: User;
  userUID: string;

  constructor(
    public navCtrl: NavController,
    private entitiesProvider: EntitiesProvider,
    private propertiesProvider: PropertiesProvider,
    private usersProvider:  UsersProvider,
    public navParams: NavParams
  ) {

  }

  ngOnInit() {
    this.userUID = this.navParams.get('userID');
    this.userSubscription = this.usersProvider.fetchUserDetails(`/users/${this.userUID}`).subscribe((userData: User) => {
        this.user = userData;
        console.log(this.user);
    });
  }

  openLink() {

  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

}
