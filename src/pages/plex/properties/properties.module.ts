import { NgModule } from '@angular/core';

import { IonicModule } from 'ionic-angular';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../../../components/components.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PropertiesListComponent } from './properties-list/properties-list';
import { PropertiesEditComponent } from './properties-edit/properties-edit';

@NgModule({
	declarations: [
    PropertiesListComponent,
    PropertiesEditComponent
  ],
	imports: [
		CommonModule, // <--- for angular directives
    IonicModule, // <--- for ionic components
    FontAwesomeModule,
    ComponentsModule
	],
  entryComponents: [
    PropertiesListComponent,
    PropertiesEditComponent
  ],
})
export class PropertiesModule {}
