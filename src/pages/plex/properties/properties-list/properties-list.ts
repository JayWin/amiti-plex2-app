import { Component, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";
import { EntitiesProvider } from "../../../../providers/entities/entities";
import { Subscription } from "rxjs/Subscription";
import { Property } from "../../../../models/property";
import { User } from "../../../../models/user";
import { PropertiesProvider } from "../../../../providers/properties/properties";
import "rxjs/add/operator/take";
import { faBuilding, faUser } from "@fortawesome/free-solid-svg-icons";
import { UsersProvider } from "../../../../providers/users/users";
import { PropertiesEditComponent } from "../properties-edit/properties-edit";

@Component({
  selector: "properties-list",
  templateUrl: "properties-list.html"
})
export class PropertiesListComponent implements OnInit {
  faBuilding = faBuilding;

  propertySubscription: Subscription;
  properties: Property[];

  constructor(public navCtrl: NavController, private entitiesProvider: EntitiesProvider, private propertiesProvider: PropertiesProvider, private usersProvider: UsersProvider) {}

  ngOnInit() {
    this.propertySubscription = this.propertiesProvider
      .fetchEntityProperties()
      .take(1)
      .subscribe(propertiesData => {
        this.properties = propertiesData;
        console.log(this.properties);
      });
  }

  openLink(propertyID) {
    console.log(propertyID);
    this.navCtrl.push(PropertiesEditComponent, {
      propertyID: propertyID
    });
  }
}
