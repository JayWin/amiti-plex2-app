import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController, NavParams } from "ionic-angular";
import { EntitiesProvider } from '../../../../providers/entities/entities';
import { Subscription } from "rxjs/Subscription";
import { Property } from '../../../../models/property';
import { User } from '../../../../models/user';
import { PropertiesProvider } from '../../../../providers/properties/properties';
import 'rxjs/add/operator/take';
import { faBuilding, faUser } from '@fortawesome/free-solid-svg-icons';
import { UsersProvider } from '../../../../providers/users/users';

@Component({
  selector: 'properties-edit',
  templateUrl: 'properties-edit.html'
})
export class PropertiesEditComponent implements OnInit, OnDestroy {

  propertySubscription: Subscription;
  property: Property;
  propertyUID: string;

  constructor(
    public navCtrl: NavController,
    private entitiesProvider: EntitiesProvider,
    private propertiesProvider: PropertiesProvider,
    private usersProvider:  UsersProvider,
    public navParams: NavParams
  ) {

  }

  ngOnInit() {
    this.propertyUID = this.navParams.get('propertyID');
    this.propertySubscription = this.propertiesProvider.fetchPropertyDetails(this.propertyUID).subscribe((propertyData: Property) => {
        this.property = propertyData;
        this.property.uid = this.propertyUID;
        console.log(this.property);
    });
  }

  openLink() {

  }

  ngOnDestroy() {
    this.propertySubscription.unsubscribe();
  }

}
