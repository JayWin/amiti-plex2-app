import { Component, OnInit, OnDestroy } from "@angular/core";
import { NavController } from "ionic-angular";
import { Subscription } from "rxjs/Subscription";

import { Entity } from "../../../models/entity";
import { EntitiesProvider } from "../../../providers/entities/entities";
import { Module } from "../../../models/management-modules.model";
import { HomePage } from "../home/home";

@Component({
  selector: "page-dashboard",
  templateUrl: "dashboard.html"
})
export class DashboardPage implements OnInit, OnDestroy {
  entity: Entity;
  entitySubscription: Subscription;
  hasEntity: boolean = false;

  modules = [];
  modulesSubscription: Subscription;

  constructor(
    public navCtrl: NavController,
    private entitiesProvider: EntitiesProvider
  ) {}

  ngOnInit() {
    const entityID = sessionStorage.getItem('entity_id');
    console.log(entityID);
    this.hasEntity = false;
    if (entityID) {
        this.hasEntity = true;
    }
    // FETCH SELECTED ENTITY
    this.entitySubscription = this.entitiesProvider.getEntity(entityID).subscribe(entityData => {
        this.entity = entityData;
        console.log(this.entity);
    });

    // SET DEFAUT MODULES
    this.modules['fin'] = false;
    this.modules['tasks'] = false;
    this.modules['communications'] = false;

    // FETCH ENTITY MODULES
    this.modulesSubscription = this.entitiesProvider.fetchEntityModules().subscribe(modulesData => {
        modulesData.forEach((moduleData: Module) => {
            this.modules[moduleData.id] = moduleData.active;
        });
    });
  }

  homeClick() {
    this.navCtrl.setRoot(HomePage);
  }

  ngOnDestroy() {
      this.entitySubscription.unsubscribe();
      this.modulesSubscription.unsubscribe();
  }
}
