import { Component, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";
import { Subscription } from "rxjs/Subscription";

import { Entity } from "../../../models/entity";
import { EntitiesProvider } from "../../../providers/entities/entities";
import { DashboardPage } from "../dashboard/dashboard";
import { AuthProvider } from "../../../providers/auth/auth";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage implements OnInit {
  entities: Entity[];
  subscription: Subscription;
  filter: any;
  loadingEntities: boolean = true;

  constructor(
    public navCtrl: NavController,
    private entitiesProvider: EntitiesProvider,
    private auth: AuthProvider
  ) {}

  ngOnInit() {

    this.auth.user.subscribe(user => {
      if (user) {
        const userID = user.uid;
        this.subscription = this.entitiesProvider.getEntities(userID).subscribe(entitiesData => {
          console.log("entities data", entitiesData);
          this.entities = entitiesData;
          this.loadingEntities = false;
        });
      }
    });

  }

  setEntity(event, entity: Entity) {
      this.entitiesProvider.setEntity(entity);
      this.navCtrl.setRoot(DashboardPage);
  }
}
